package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText tela;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tela = findViewById(R.id.tela_textView);
    }

    public void addText(String telaText){
        String conta = tela.getText().toString();
        tela.setText(String.format("%s%s",conta,telaText));
    }

    public void zeroBTN(View view){
        addText("0");
    }
    public void umBTN(View view){
        addText("1");
    }
    public void doisBTN(View view){
        addText("2");
    }
    public void tresBTN(View view){
        addText("3");
    }
    public void quatroBTN(View view){
        addText("4");
    }
    public void cincoBTN(View view){
        addText("5");
    }
    public void seisBTN(View view){
        addText("6");
    }
    public void seteBTN(View view){
        addText("7");
    }
    public void oitoBTN(View view){
        addText("8");
    }
    public void noveBTN(View view){
        addText("9");
    }
    public void limparBTN(View view){
        tela.setText("");
    }
    public void dividirBTN(View view){
        addText("/");
    }
    public void multiplicarBTN(View view){
        addText("*");
    }
    public void somarBTN(View view){
        addText("+");
    }
    public void subtrairBTN(View view){
        addText("-");
    }
    public void pontoBTN(View view){
        addText(".");
    }
    public void igualBTN(View view){

    }
    public void apagarUmBTN(View view){
        int contaLenght = tela.getText().length();
        String currText = tela.getText().toString();

        if(contaLenght != 0){
            currText = currText.substring(0,contaLenght - 1);
            tela.setText(currText);
        }
    }


}